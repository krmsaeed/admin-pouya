import React from 'react';
import ReactDOM from 'react-dom';
import thunk from 'redux-thunk';
import { createStore, compose, applyMiddleware } from 'redux';
import reducers from './redux/reducers';
import { Provider } from 'react-redux';
import App from './App';
// import "@syncfusion/ej2-base/styles/bootstrap-dark.css";
// import "@syncfusion/ej2-navigations/styles/bootstrap-dark.css";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

ReactDOM.render(
    <Provider store={createStore(reducers, composeEnhancers(applyMiddleware(thunk)))}>
        <App />
    </Provider>
    , document.getElementById('root'));
