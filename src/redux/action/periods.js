import axios from 'axios'
import * as types from '../types/periodsType'


export const getAllPeriods = (data) => (dispatch) => {
    dispatch({
        type: types.GET_ALL_PERIODS,
        payload: data
    })
}
export const addPeriods = (add) => (dispatch) => {
    dispatch({
        type: types.ADD_PERIODS,
        payload: add
    })
}