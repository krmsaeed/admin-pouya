import * as types from '../types/periodsType';

let initialState ={
    data:[
        {"Number":1,"Year":1396},
        {"Number":2,"Year":1397},
        {"Number":3,"Year":1398},
        {"Number":4,"Year":1399},
        {"Number":5,"Year":1400},
   ]
}

export const periodReducer = (state =initialState, action) => {
    
    switch (action.type) {
        case types.GET_ALL_PERIODS:
            return {
                ...state,
                data: action.payload
            }
            case types.ADD_PERIODS:
            return {
                ...state,
                data: console.log(action.payload)
            }
            default:
                return state;
    }
}

export default periodReducer;