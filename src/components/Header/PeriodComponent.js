import React, { Component } from 'react';
import { PeriodServiceBase } from '../Services/PeriodService';
import Locator from 'locustjs-locator';
import { ButtonComponent } from '@syncfusion/ej2-react-buttons';

export default class PeriodComponent extends Component {
    constructor() {
        super();
        this.periodService = Locator.Instance.resolve(PeriodServiceBase);
        this.state = {
            periods: [],
            year: ''
        }
       
    }
  
    async componentDidMount() {
        const periods = await this.periodService.getAll();

        this.setState({ periods });
    }
    add = async () => {
        const periods = await this.periodService.add();

        this.setState({ periods })

        if (typeof this.props.onAdded == 'function') {
        }
    }
    delete = async () => {
        const { year } = this.state;

        try {
            const periods = await this.periodService.deleteByYear(year);

            this.setState({ periods })

            if (typeof this.props.onDeleted == 'function') {
                this.props.onDeleted(periods);
            }
        } catch (e) {
            alert(`Cannot delete year ${year}`)
            console.log(e);
        }
    }
    handleChangeYear = () => {
        this.props.yearData(this.state.year)
    }
    render() {
        const { periods } = this.state;
        return <div>
            <div className="sk_modal_btn_top">
                <ButtonComponent cssClass='e-button' onClick={this.delete}><i className="e-icons e-minus"></i></ButtonComponent>
                <ButtonComponent cssClass='e-button' onClick={this.add}><i className="e-icons e-plus"></i></ButtonComponent>
            </div>
            <div className="sk_modal_content">
                <div className="sk_modal_content_title"><span>دوره مالی</span></div>
                {
                    periods.map((item, i) => {
                        return (
                            <button key={item.Number} onClick={e => this.setState({ year: e.target.innerText })} className="sk_period_item">{item.Year}</button>
                        )

                    })
                }
            </div>
            <div className="sk_modal_footer_button" >
                <ButtonComponent cssClass='e-button' onClick={this.handleChangeYear} >تکمیل</ButtonComponent>
                <ButtonComponent cssClass='e-button' onClick={() => { console.log('hhii') }}>انصراف</ButtonComponent>

            </div>
        </div>
    }
}
