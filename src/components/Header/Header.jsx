import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    MenuComponent,
   

} from '@syncfusion/ej2-react-navigations';
import { DialogUtility} from '@syncfusion/ej2-popups';
import { enableRipple } from '@syncfusion/ej2-base';
import Locator from 'locustjs-locator';
import { getAllPeriods, addPeriods } from '../../redux/action/periods'
import { PeriodServiceBase } from '../Services/PeriodService';
import './sass/Header.scss'
import PeriodComponent from './PeriodComponent'

enableRipple(true);

class Navbar extends Component {
    constructor(props) {
        super(props);

        this.periodService = Locator.Instance.resolve(PeriodServiceBase);

        this.state = {
            periodss: [],
            year: '1399'
        }
        this.buttons = [{
            buttonModel: {
                content: 'Close',
                cssClass: 'e-flat',
                isPrimary: true,
            },
            'click': () => {
                this.dialogInstance.hide();
            }
        }];
        this.settings = { effect: 'Zoom', duration: 400, delay: 0 };
    }

handleChildData=(data)=>{
    this.setState({year: data})
}
    async buttonClick() {
        DialogUtility.alert({
            animationSettings: { effect: 'Zoom' },
            closeOnEscape: true,
            content: () => {
                return (<div>
                    <PeriodComponent yearData={this.handleChildData} onAdded={() => console.log('added')} onHide={()=>{console.log('hide')}} onDeleted={() => console.log('deleted')} />
                   
                </div>)
            },
           
            okButton: { text: '' },
            showCloseIcon: true,
            title: 'دوره مالی',

        });
    }
    select = (args) => {
        if (args.item.id === '2') {
            this.buttonClick();
        }
    }

    render() {
        console.log('',this)
        const menuItems = [
            {
                id: '1',
                text: 'خروج از سیستم'
            },
            {
                // iconCss: 'em-icons e-edit',
                id: '2',
                text: `دوره مالی : ${this.state.year}`,
            },
            {
                id: '3',
                text: 'کاربر: طرح انديشان'
            },
            {
                id: '4',
                items: [
                    { text: 'خالی کردن حافظه پنهان', iconCss: 'em-icons e-open' },
                    { text: 'راه اندازی مجدد نرم افزار', iconCss: 'e-icons e-save' },
                ],
                text: '. . .'
            },
        ];
        return (
            <div style={{ width: "100%" }}>
                <div style={{ direction: 'rtl' }}>
                    <div className="toolbar-menu-control">
                        <span id="hamburger" onClick={this.props.toggle} className="e-icons menu default"></span>
                    </div>
                </div>
                <MenuComponent items={menuItems} select={this.select} isResponsive={true}></MenuComponent>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        period: state.period
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        getAllPeriods: () => dispatch(getAllPeriods()),
        addPeriods: () => dispatch(addPeriods()),
    }
};
//  Navbar = withRouter(Navbar)
export default connect(mapStateToProps, mapDispatchToProps)(Navbar)