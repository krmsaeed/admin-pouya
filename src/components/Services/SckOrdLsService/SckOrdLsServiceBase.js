export default class SkOrdLsServiceBase {
    constructor() {
        if (this.constructor === SkOrdLsServiceBase) {
            throw 'SkOrdLsServiceBase is an abstract class. You cannot instantiate from it.'
        }
      }
      getAll() {
        throw 'getAll() is not implemented'
      }
}