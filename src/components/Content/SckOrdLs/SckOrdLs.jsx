import React, { Component } from 'react';
import { ComboBoxComponent } from '@syncfusion/ej2-react-dropdowns';
import { ButtonComponent } from '@syncfusion/ej2-react-buttons';
import { Edit, Grid, Toolbar, Page , ToolbarItems, PageSettingsModel, ExcelExport, ExcelExportProperties, } from '@syncfusion/ej2-react-grids';
import { L10n } from '@syncfusion/ej2-base';
import TapGrid from '../../TapGrid/TapGrid'
import './sass/SckOrdLs.scss';
import { data } from './datasource';
L10n.load({
  'fa-IR': {
    'grid': {
      'EmptyDataSourceError': 'يجب أن يكون مصدر البيانات فارغة في التحميل الأولي منذ يتم إنشاء الأعمدة من مصدر البيانات في أوتوجينيراتد عمود الشبكة',
      'EmptyRecord': 'لا سجلات لعرضها'
    },
    'pager': {
      'currentPageInfo': '{0} از {1} صفحه ',
      'firstPageTooltip': 'انتقال به صفحه یک',
      'lastPageTooltip': 'انتقال به صفحه آخر',
      'nextPageTooltip': 'انتقال به صفحه بعد',
      'nextPagerTooltip': 'الذهاب إلى بيجر المقبل',
      'previousPageTooltip': 'انتقل إلى الصفحة السابقة',
      'previousPagerTooltip': 'الذهاب إلى بيجر السابقة',
      'totalItemsInfo': '({0} آیتم)'
    }
  }
});


export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    }
    this.editOptions = { allowAdding: true, allowEditing: false, allowDeleting: true };
    this.toolbarOptions = ['Search', 'ExcelExport', 'Print'];
    this.pageOptions = { pageSize: 7 };
    this.grid = null;
   this.sortFormatData= [
    { Class: 'asc-sort', Type: 'Sort A to Z', Id: '1' },
    { Class: 'dsc-sort', Type: 'Sort Z to A ', Id: '2' },
    { Class: 'filter', Type: 'Filter', Id: '3' },
    { Class: 'clear', Type: 'Clear', Id: '4' }
  ];
  this.pageOptions = {
    pageSize: 8, pageSizes: true
  };
 this.fields= { text: 'Type', iconCss: 'Class', value: 'Id'};
  }
  toolbarClick = (args) => {
    if (this.grid && args.item.id === 'grid_excelexport') {
      const excelExportProperties = {
        fileName: "new.xlsx"
      };
      this.grid.excelExport(excelExportProperties);
    }
  }
  render() {
    return (
      <div className="sk_table_SckOrdLs">
        <div className="sk_combobox_SckOrdLs">
        <ComboBoxComponent id="comboelement" dataSource={this.sortFormatData} fields={this.fields} placeholder="انتخاب نمایید" />
        <ButtonComponent>تایید</ButtonComponent>
        </div>
        <TapGrid
          sourceData={data}
          editOptions={this.editOptions}
          toolbarOptions={this.toolbarOptions}
          enableRtl={true}
          locale='fa-IR'
          allowPaging={true}
          pageSettings={this.pageOptions}
          setting={[Edit, Grid, Toolbar,Page,]}
          toolbarClick={this.toolbarClick}
          allowExcelExport={true}
          ref={(scope) => { this.grid = scope; }}
          allowSorting={true}
          pageSettings={this.pageOptions}
          columns={[
            { field: '', headerText: '', width: '50', textAlign: 'Center', isPrimaryKey: true },
            { field: '', headerText: '', width: '50', textAlign: 'Center' },
            { field: 'OrderID', headerText: 'شماره', width: '100', textAlign: 'Center'},
            { field: 'OrderDate', headerText: 'تاریخ', width: '100', textAlign: 'Center'},
            { field: 'CustomerID', headerText: 'شماره فرعی', width: '120', textAlign: 'Center' },
            { field: 'ShipName', headerText: 'صادر کننده', width: '150', textAlign: 'Center' },
            { field: 'ShipCountry', headerText: 'نوع درخواست', width: '150', textAlign: 'Center' },
            { field: 'title', headerText: 'عنوان', width: '200', textAlign: 'Center' },
            { field: 'ShipPostalCode', headerText: 'طرف حساب', width: '150', textAlign: 'Center' },
            { field: 'ShipCountry', headerText: 'جمع کنترلی', width: '150', textAlign: 'Center' },
          ]} /> 
      </div>
    )
  }
};

