import React, { Component } from 'react'
import {  Route,  Switch, withRouter } from 'react-router-dom';
import {
    SidebarComponent,
    TreeViewComponent,

    MenuComponent,
    

} from '@syncfusion/ej2-react-navigations';
import Home from '../Content/Home/Home';
import Inbox from '../Content/SckOrdLs/SckOrdLs';
import SckOrdLs from '../Content/SckOrdLs/SckOrdLs';
import Snooze from '../Content/Snooze/Snooze';

import Header from '../Header/Header'
import './sass/Sidebar.scss'

class Sidebar extends Component {

    constructor(props) {
        super(props);

        
        this.sidebarItems = [{
            "nodeId": "00",
            "nodeText": "About",
            "url": "/"
        },
        {
            "nodeId": "01",
            "nodeText": "React",
            "url": "",
            "nodeChild": [{
                "nodeId": "01-01",
                "nodeText": "Javascript",
                "url": "/Javascript",
            }]
        },
        {
            "nodeId": "02",
            "nodeText": "Products",
            "url": "",
            "expanded": true,
            "nodeChild": [{
                "nodeId": "02-01",
                "nodeText": "Services",
                "url": "/topics"
            }]
        }]

        this.menuFields = {
            children: ['subItems', 'options'],
            text: ['header', 'text', 'value']
        };
        this.data = [
          
            {
                nodeId: "01", nodeText: "صفحه اصلی", iconCss: "sf-icon-sidebar sf-icon-file", url: "/Admin"
            },
            {
                nodeId: "02", nodeText: "درخواست ها", iconCss: "sf-icon-sidebar sf-icon-recent", url: "",
                nodeChild: [
                    { nodeId: "02-01", nodeText: "درخواست ورود کالا", iconCss: "sf-icon-sidebar sf-icon-recent", url: "/Admin/SckOrdLs", },
                    { nodeId: "02-02", nodeText: "درخواست خروج کالا", iconCss: "sf-icon-sidebar sf-icon-recent", url: "/Admin/SckReqLs", },
                    { nodeId: "02-03", nodeText: "خدمات", iconCss: "sf-icon-sidebar sf-icon-recent", url: "/Admin/SckSvcLs", },
                ]
            },

            {
                nodeId: "03", nodeText: "دستور خرید", iconCss: "icon-docs icon", url: "",
                nodeChild: [
                    { nodeId: "03-01", nodeText: "مدیریت دستور خرید", iconCss: "icon-circle-thin icon", url: "", },
                    { nodeId: "03-02", nodeText: "اسناد دستور خرید کالا", iconCss: "icon-circle-thin icon", url: "", },
                ]
            },


        ];
        this.toggleClick = this.toggleClick.bind(this);
        this.onCreate = this.onCreate.bind(this);
        this.width = '290px';
        this.target = '.main-content';
        this.mediaQuery = '(min-width: 600px)';
        this.dockSize = "80px";
        this.animation = { effect: 'None' };
        this.onCreated = this.onCreated.bind(this);
        this.menuTemplate = this.menuTemplate.bind(this);
        this.fields = { dataSource: this.data, id: 'nodeId', text: 'nodeText', child: 'nodeChild' };
        this.cssClass = "e-text";
        this.tree = null;
        // this.isOpenSidebar= true
        this.state = {
            isOpenSidebar: true
        }
    }


    menuTemplate() {
        return <MenuComponent id="menu" items={this.menuItems} fields={this.menuFields} animationSettings={this.animation} />
    }
    onCreated() {
        this.tbObj.refreshOverflow();
        this.sidebarobj.element.style.visibility = '';
    }
    // onClose = () => {
    //     this.tree.collapseAll();
    // }

    toggleClick() {
        if (this.sidebarobj.isOpen) {
            this.sidebarobj.hide();
            this.tree.collapseAll();
            this.setState({
                isOpenSidebar: !this.state.isOpenSidebar
            })
        }
        else {
            this.sidebarobj.show();
            // this.tree.expandAll();
            this.setState({
                isOpenSidebar: !this.state.isOpenSidebar
            })

        }

    }
    onCreate() {
        this.sidebarobj.element.style.visibility = '';

    }
    nodeTemplate = (data) => {

        return (
            <div className="sk_sidebar_subtitle">
                <span className="" >{data.nodeText}</span>
                <i className={data.iconCss} ></i>
            </div>
        )

    }
    loadPage = (args) => {
        var data = this.tree.getTreeData(args.node);
        let routerLink = data[0].url;
        if (routerLink !== "") {

            this.props.history.push(routerLink);
        }
    }
    
    render() {
        let classItem = ""
        if (this.state.isOpenSidebar) {
            classItem = "sk_show_node_text"
        } else {
            classItem = "sk_hide_node_text"
        }
        return (
            <div className="control-section">
                <SidebarComponent position="Right" id="default-sidebar" ref={Sidebar => this.sidebarobj = Sidebar} width={this.width} iconCss='e-icons burg-icon' isToggle={true} style={{ visibility: "hidden" }} created={this.onCreate} close={this.onClose} dockSize={this.dockSize} enableDock={true}>
                    <div className="sidebar-header header-cover" style={{ backgroundColor: '#0378d5' }}>
                        <div className="image-container">
                            <div className="sidebar-image" style={{ backgroundPosition: !this.isOpenSidebar ? '100%' : 'center' }}>
                            </div>
                        </div>
                    </div>
                    <div className={classItem}>
                        <TreeViewComponent fields={this.fields} cssClass={this.cssClass} ref={treeview => { this.tree = treeview }} nodeTemplate={this.nodeTemplate} nodeSelected={this.loadPage} expandOn='Click' />
                    </div>
                </SidebarComponent>



                <div style={{ direction: 'rtl' }}>
                <Header toggle={this.toggleClick}/>
                    <div className="sk_content_route">
                        <Switch>

                            <Route path="/Admin" component={Home} exact />
                            <Route path="/Admin/SckOrdLs" component={Inbox} exact />
                            <Route path="/Admin/SckReqLs" component={SckOrdLs} exact />
                        </Switch>
                    </div>

                </div>
            </div>
        );
    }
}


export default withRouter(Sidebar) 
