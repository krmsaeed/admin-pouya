import React, { Component } from 'react'
import { ColumnDirective, GridComponent, ColumnsDirective,  Inject } from '@syncfusion/ej2-react-grids';


export default class TapGrid extends Component {
constructor(props){
    
    super(props);
    this.state={
        
    }
}
   render() {
        return (
            <div>
                <div id="GridParent">
                    <GridComponent 
                    dataSource={this.props.sourceData} 
                    enableRtl= {this.props.enableRtl} 
                    locale={this.props.locale} 
                    page={this.props.pagination} 
                    editSettings={this.props.editOptions} 
                    toolbar={this.props.toolbarOptions}
                    allowPaging={this.props.allowPaging} 
                    pageSettings={this.props.pageSettings}
                    toolbarClick={this.props.toolbarClick}
                    allowExcelExport={this.props.allowExcelExport}
                    allowSorting={this.props.allowSorting}
                    pageSettings={this.props.pageSettings}
                    ref={this.props.ref}
                         >
                        <ColumnsDirective>
                            {this.props.columns.map((c,i) => <ColumnDirective {...c} key={i}/>)}
                        </ColumnsDirective>
                        <Inject services={this.props.setting} />
                    </GridComponent>
                </div>
            </div>
        )
    }
}
