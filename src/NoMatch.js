import React from 'react';
import {  Redirect } from 'react-router-dom'

const NoMatch = ({ location }) => (
    <div>
        <h4 className="text-center mt-5">
            !صفحه‌ای که دنبال آن بودید پیدا نشد
        </h4>
        <p className="text-center">
            <Redirect to="/main" />
        </p>
    </div>
);

export default NoMatch;