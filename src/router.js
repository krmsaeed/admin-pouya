import React from "react";
import { Switch, Route, withRouter } from "react-router-dom";
import { TransitionGroup, CSSTransition } from "react-transition-group";
import Notifications from 'react-notify-toast';

import './App.css';


// import NoMatch from './NoMatch'
import Sidebar from './components/Sidebar/Sidebar';





function router({ location }) {
  return (
    <TransitionGroup>
      <CSSTransition timeout={200} classNames="my-node">
        <>
          <Switch>
            <Route path="/Admin" component={Sidebar} />
          </Switch>
          <Notifications options={{ zIndex: 9999, top: '90%' }} />
        </>
      </CSSTransition>
    </TransitionGroup>
  );
}

export default withRouter(router);
